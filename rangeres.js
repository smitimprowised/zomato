const csv = require('csv-parser');
var fs = require("fs");

var raw_data = require("./readcsv.js")

function CSVtoArray(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    var a = [];                     // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

var lines=raw_data.split("\n");

  var result = [];

  var headers=lines[0].split(",");


  for(var i=1;i<lines.length;i++){

	  var obj = {};
	  var currentline = CSVtoArray(String(lines[i])); 
   
	  for(var j=0;j<headers.length;j++){
		  obj[headers[j]] = currentline[j];
	  }
   
	  result.push(obj);
    
  } 
var counter =1;
  result.forEach((value) => {
   if (process.argv[2] > value.Longitude && process.argv[3]<value.Latitude && counter<10)
    {console.log(value.Restaurant_Name)
    counter++;
    }
  }) 